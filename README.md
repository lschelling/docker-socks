# DockerSOCKD

Basic Docker container with the Dante SOCKS server!

Based on https://github.com/Ejz/DockerSOCKD

The socks server runs in anonymous mode, no user/password required.

The socks server is configured to support tcp and udp protocol.

The 'internal' interface is eth1 the external interface eth0.

### Quick start

Build the image:
~~~~
$ sudo docker build --tag="lorenz/sockd" 'bitbucket.com/lschelling/docker-socks'
Successfully built 40f179bee5a9
~~~~

Run container based on this image:

~~~~
$ sudo docker run -d --net="host" lorenz/sockd
d80c9e34575eb16fb1b34d6173a2a7f1837735b2f4e7580ac4e9cd8986c38a3c
~~~~

Test it:

~~~~
$ curl --socks5 127.0.0.1:1080 'http://www.google.com' 10.0.0.1